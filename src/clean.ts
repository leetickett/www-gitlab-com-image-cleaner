import * as yargs from 'yargs';
import * as path from 'path';
import * as fs from 'fs';
import * as glob from 'glob';
import * as yaml from 'yaml';
import * as chalk from 'chalk';

const { argv } = yargs
  .usage('Usage: ts-node clean.ts path/to/www-gitlab-com/repo/root')
  .alias('p', 'repo-path')
  .describe('repo-path', 'Path to your local www-gitlab-com repo')
  .demandOption(['repo-path']);

const repoPath = path.resolve(process.cwd(), argv.repoPath as string);

const teamYmlPath = path.resolve(repoPath, 'data/team.yml');
const petsYmlPath = path.resolve(repoPath, 'data/pets.yml');
const teamImagesPath = path.resolve(repoPath, 'source/images/team');
const petImagesPath = path.resolve(repoPath, 'source/images/team/pets');

const allTeamImagePaths = glob.sync(path.join(teamImagesPath, '/*'), {
  nodir: true,
});
const allPetImagePaths = glob.sync(path.join(petImagesPath, '/*'), {
  nodir: true,
});

const teamYmlString = fs.readFileSync(teamYmlPath, 'utf8');
const teamYml = yaml.parse(teamYmlString);

const imagesReferencedFromTeamYml: string[] = teamYml.map(
  (member: any) => member.picture,
);

const petsYmlString = fs.readFileSync(petsYmlPath, 'utf8');
const petsYml = yaml.parse(petsYmlString);

const imagesReferencedFromPetsYml: string[] = petsYml.map(
  (pet: any) => pet.picture,
);

let teamImagesDeleted = 0;
for (const teamImagePath of allTeamImagePaths) {
  if (!imagesReferencedFromTeamYml.includes(path.basename(teamImagePath))) {
    console.log(`Deleting ${chalk.red(teamImagePath)}...`);
    fs.unlinkSync(teamImagePath);
    teamImagesDeleted++;
  }
}

let petImagesDeleted = 0;
for (const petImagePath of allPetImagePaths) {
  if (!imagesReferencedFromPetsYml.includes(path.basename(petImagePath))) {
    console.log(`Deleting ${chalk.red(petImagePath)}...`);
    fs.unlinkSync(petImagePath);
    petImagesDeleted++;
  }
}

console.log(
  chalk.green(
    `✅ Done checking ${allTeamImagePaths.length} team photos and ${allPetImagePaths.length} pet photos`,
  ),
);

console.log(
  `Deleted ${chalk.blue(teamImagesDeleted)} team photos and ${chalk.blue(
    petImagesDeleted,
  )} pet photos`,
);
